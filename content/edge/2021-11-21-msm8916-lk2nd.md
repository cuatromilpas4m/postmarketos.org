title: "MSM8916 devices: Update lk2nd to get all CPU cores"
date: 2021-11-21
---

The `linux-postmarketos-qcom-msm8916` 5.15 update was merged for all the
[Qualcomm Snapdragon 410 (MSM8916) based devices in postmarketOS](https://wiki.postmarketos.org/wiki/Qualcomm_Snapdragon_410/412_(MSM8916)).
It introduces a new way to select which firmware you want to enable using pmbootstrap:

     $ pmbootstrap init
     [...]
     Available providers for soc-qcom-msm8916-rproc (3):
      * all: Enable all remote processors (audio goes through modem) (**default**)
      * no-modem: Disable only modem (audio bypasses modem, ~80 MiB more RAM)
      * none: Disable all remote processors (no WiFi/BT/modem, ~90 MiB more RAM)
     Provider [default]: ...

Please make sure that you:

  1. Install [lk2nd 0.11.0](https://github.com/msm8916-mainline/lk2nd/releases/tag/0.11.0).
     **With the old lk2nd only one of the CPU cores will be started!**
     You can update lk2nd from the lk2nd fastboot screen:

         $ fastboot flash lk2nd lk2nd-msm8916.img

  2. Update to pmbootstrap 1.39.0 if you want to see the new selection shown above.
     Older pmbootstrap versions will default to enabling all remote processors.

If you are updating an existing system, note that all installations will be converted
to full functionality (modem enabled), even if you previously selected the `mainline`
kernel variant without modem functionality. If you would like to change the selection
on a running system you can explicitly install the new `no-modem` variant using:

    $ sudo apk add soc-qcom-msm8916-rproc-no-modem
